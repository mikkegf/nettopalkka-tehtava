<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Nettopalkkaa</title>
</head>

<body>
    <div class="container">
        <h3>Nettopalkan laskenta</h3>
        <form action="<?php print($_SERVER['PHP_SELF']);?>" method="post">
            <div class="form-group row">
                <label for="colFormLabelSm" class="col-5 col-form-label col-form-label-sm">Bruttopalkka:</label>
                <div class="col-3">
                    <input type="number" class="form-control form-control-sm" name="brutto" step="0.01" required>
                </div>
                <span>
                    <?php
                        if ($_SERVER['REQUEST_METHOD']=='POST'){
                        $brutto = filter_input(INPUT_POST, 'brutto', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                        printf($brutto . " € ");
                        }
                    ?>
                </span>

            </div>
            <div class="form-group row">
                <label for="colFormLabelSm" class="col-5 col-form-label col-form-label-sm">Ennakkopidätys:</label>
                <div class="col-3">
                    <input type="number" class="form-control form-control-sm" name="vero" step="0.01" required>
                </div>
                <span>%</span>
                <span class="col">
                    <?php
                        if ($_SERVER['REQUEST_METHOD']=='POST') {
                        $vero = filter_input(INPUT_POST, 'vero', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                        $ennakko = ($vero / 100) * $brutto;
                        printf("%.2f €", $ennakko);
                        }
                    ?>
                </span>
            </div>
            <div class="form-group row">
                <label class="col-5 col-form-label col-form-label-sm">Työeläkemaksu:</label>
                <div class="col-3">
                    <input type="number" class="form-control form-control-sm" name="elake" step="0.01" required>
                </div>
                <span>%</span>
                <span class="col">
                    <?php
                        if ($_SERVER['REQUEST_METHOD']=='POST') {
                        $elake = filter_input(INPUT_POST, 'elake', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                        $tyoelake = ($elake / 100) * $brutto;
                        printf("%.2f €", $tyoelake);
                        }
                    ?>
                </span>
            </div>
            <div class="form-group row">
                <label class="col-5 col-form-label col-form-label-sm">Työttömyysvakuutusmaksu:</label>
                <div class="col-3">
                    <input type="number" class="form-control form-control-sm" name="vakuutus" step="0.01" required>
                </div>
                <span>%</span>
                <span class="col">
                    <?php
                        if ($_SERVER['REQUEST_METHOD']=='POST') {
                        $vakuutus = filter_input(INPUT_POST, 'vakuutus', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                        $vakuutusmaksu = ($vakuutus / 100) * $brutto;
                        printf("%.2f €", $vakuutusmaksu);
                        }
                    ?>
                </span>
            </div>
            <button class="btn btn-primary">Laske</button>
            <div class="row">
                <p class="col">
                    <?php
                        if ($_SERVER['REQUEST_METHOD']=='POST') {
                        $nettopalkka = $brutto - $ennakko - $tyoelake - $vakuutusmaksu;
                        printf(" Nettopalkka on: %.2f € ", $nettopalkka);
                        }
                    ?>
                </p>    
            </div>
        </form>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>
</body>

</html>